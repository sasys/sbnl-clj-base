#
#
#
# Docker file describes the container for the sa project REST service
#
FROM clojure:lein-2.8.1

RUN ["apt-get", "update"]
RUN ["apt-get", "install", "-y", "vim"]
RUN ["apt-get", "install", "-y", "rlwrap"]
